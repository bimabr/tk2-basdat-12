from django.shortcuts import render, redirect
from django.db import connection
from datetime import datetime

# Create your views here.
def home(request):
    return render(request, 'biru.html')

def qkurir(request):
    return render(request, 'birukurir.html')

def qstaff(request):
    return render(request, 'birustaff.html')

def qpelanggan(request):
    return render(request, 'birupelanggan.html')

def staff(request):
    if 'is_login' in request.session:
        if request.session['user_type'] == 'Pelanggan':
            email = request.session['email']
            query = f"select * from transaksi_topup_dpay where email = '{email}'"
            with connection.cursor() as cursor:
                cursor.execute(query)
                dummy = cursor.fetchall()
        else:
            with connection.cursor() as cursor:
                cursor.execute('select * from transaksi_topup_dpay')
                dummy = cursor.fetchall()
    else:
        with connection.cursor() as cursor:
            cursor.execute('select * from transaksi_topup_dpay')
            dummy = cursor.fetchall()
    context = {
        'dummy' : dummy
    }
    return render(request, 'staff.html', context)

def kurir(request):
    with connection.cursor() as cursor:
        cursor.execute('select * from transaksi_topup_dpay')
        dummy = cursor.fetchall()
    context = {
        'dummy' : dummy
    }
    return render(request, 'kurir.html', context)

def pilihEmail(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            query = f"select * from transaksi_topup_dpay where email ='{request.POST['email']}'"
            cursor.execute(query)
            dummy = cursor.fetchall()
        context = {
            'dummy' : dummy
        }
        return render(request, 'staff.html', context)
    else:
        with connection.cursor() as cursor:
            cursor.execute('select email from transaksi_topup_dpay')
            dummy = cursor.fetchall()
        context = {
            'dummy' : dummy
        }
        return render(request, 'emailPengguna.html', context)

def delete(request):
    if request.method == 'POST':
        tgl = request.POST['tanggal']
        query = f"delete from transaksi_topup_dpay where tanggal='{tgl}'"
        with connection.cursor() as cursor:
            cursor.execute(query)
        return redirect('biru:staff')


def update(request):
    if request.method == 'POST':
        email = request.POST['email']
        tanggal = request.POST['date']
        nominal = request.POST['nominal']
        query = f"update transaksi_topup_dpay set nominal = {nominal}  where email = '{email}' and tanggal = '{tanggal}'";
        with connection.cursor() as cursor:
            cursor.execute(query)
    return redirect('biru:staff')


def create(request):
    if request.method == 'POST':
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
        query = f"INSERT INTO transaksi_topup_dpay VALUES ('{request.POST['email']}','{dt_string}','{request.POST['nominal']}')"
        with connection.cursor() as cursor:
            cursor.execute(query)
            cursor.execute('select * from transaksi_topup_dpay')
            dummy = cursor.fetchall()
        context = {
            'dummy' : dummy
        }
        return render(request, 'staff.html', context)
    else:
        with connection.cursor() as cursor:
            cursor.execute('select email from transaksi_topup_dpay')
            dummy = cursor.fetchall()
        context = {
            'dummy' : dummy
        }
        return render(request, 'createTransaksi.html', context)

def status(request):
    with connection.cursor() as cursor:
        cursor.execute('select * from status')
        dummy = cursor.fetchall()
    context = {
        'dummy' : dummy
    }
    return render(request, 'status.html', context)