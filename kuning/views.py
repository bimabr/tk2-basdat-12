from django.shortcuts import render, redirect
from django.db import connection

from .forms import TarifAntarJemputForm, DummyRoleForm


# global dummy_tarif 
# dummy_tarif = [['DSTNC1', 0, 3, 5000.0], ['DSTNC2', 3, 10, 10000.0]]


def home(request):
	# if 'dummy_role' not in request.session:
	# 	request.session['dummy_role'] = 'pelanggan'
	dummy_role = "kurir"
	if request.method == 'POST':
		form = DummyRoleForm(request.POST)
		if form.is_valid():
			dummy_role = form.cleaned_data['dummy_role']
			# request.session['dummy_role'] = dummy_role
			# return redirect('/kuning/')
	# dummy_role = request.session['dummy_role']
	form = DummyRoleForm(initial={'dummy_role': dummy_role})
	# context = {'form': form, 'is_staf': (request.session['dummy_role'] == 'staf')}
	context = {'form': form, 'is_staf': True}
	return render(request, 'kuning/dummy_role.html', context)



def read_tarif(request):
	with connection.cursor() as cursor:
		cursor.execute(f'select * from tarif_antar_jemput')
		dummy_tarif = cursor.fetchall()

	# context = {'tarif_antar_jemput': dummy_tarif, 'is_staf': (request.session['dummy_role'] == 'staf')}
	context = {'tarif_antar_jemput': dummy_tarif, 'is_staf': True}

	return render(request, 'kuning/rud_tarif.html', context)

def create_tarif(request):
	form = TarifAntarJemputForm()
	context = {'form': form}
	if request.method == 'POST':
		form = TarifAntarJemputForm(request.POST)
		if form.is_valid():
			kode = form.cleaned_data['kode']
			jarak_min = form.cleaned_data['jarak_min']
			jarak_max = form.cleaned_data['jarak_max']
			harga = form.cleaned_data['harga']
			# dummy_tarif.append([kode, jarak_min, jarak_max, harga])
			with connection.cursor() as cursor:
				query = f'INSERT INTO "tarif_antar_jemput" VALUES  (\'{kode}\', {jarak_min}, {jarak_max}, {harga})'
				cursor.execute(query)
				# print(cursor.fetchall())
			return redirect("/kuning/read_tarif")

	return render(request, 'kuning/create_tarif.html', context)



def update_tarif(request, kode_update):
	# init_form = {'kode': tarif[0], 'jarak_min': tarif[1], 'jarak_max': tarif[2], 'harga': tarif[3]}
	init_form = None
	with connection.cursor() as cursor:
		query = f'select * from tarif_antar_jemput where kode = \'{kode_update}\''
		cursor.execute(query)
		tarif = cursor.fetchall()
		init_form = {'kode': tarif[0][0], 'jarak_min': tarif[0][1], 'jarak_max': tarif[0][2], 'harga': tarif[0][3]}
	form = TarifAntarJemputForm(initial=init_form)
	context = {'form': form}
	if request.method == 'POST':
		form = TarifAntarJemputForm(request.POST)
		if form.is_valid():
			kode = form.cleaned_data['kode']
			jarak_min = form.cleaned_data['jarak_min']
			jarak_max = form.cleaned_data['jarak_max']
			harga = form.cleaned_data['harga']

			# dummy_tarif[idx] = [kode, jarak_min, jarak_max, harga]
			with connection.cursor() as cursor:
				cursor.execute(f"UPDATE tarif_antar_jemput SET kode='{kode}' AND jarak_min={jarak_min} \
				AND jarak_max={jarak_max} AND harga={harga} WHERE kode={kode}")

			return redirect("/kuning/read_tarif")

	return render(request, 'kuning/create_tarif.html', context)




def delete_tarif(request, kode_delete):
	query = f'DELETE FROM tarif_antar_jemput WHERE kode=\'{kode_delete}\''
	with connection.cursor() as cursor:
		cursor.execute(query)

	return redirect("/kuning/read_tarif")




def read_profil(request):
	dummy_role = request.session['dummy_role']
	dict_fields = {'pelanggan': ['Email', 'Password', 'Nama Lengkap', 'Alamat', 'No HP', 
								'Jenis Kelamin', 'No Virtual Account', 'Saldo Dpay'],
					'staf': ['Email', 'Password', 'Nama Lengkap', 'Alamat', 'No HP', 
							'NPWP', 'No Rekening', 'Nama Bank', 'Kantor Cabang'],
					'kurir': ['Email', 'Password', 'Nama Lengkap', 'Alamat', 'No HP', 
							'NPWP', 'No Rekening', 'Nama Bank', 'Kantor Cabang',
							'No SIM', 'No Kendaraan', 'Jenis Kendaraan']
	}

	# email, password, nama_lengkap, alamat, no_hp, jenis_kelamin,
	# no_virtual_account, saldo_dpay, 
	# npwp, no_rekening, nama_bank, kantor_cabang,
	# npwp, no_rekening, nama_bank, kantor_cabang, no_sim, no_kendaraan, jenis_kendaraan
	context = {'dummy_role': dummy_role,
				'fields_profile': dict_fields[dummy_role]}

	return render(request, 'kuning/read_profil.html', context)
