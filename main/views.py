from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.contrib.auth.models import User

from .forms import Login, Register, CustomerRegister,  CourierRegister, StaffRegister

def home(request):

    return redirect('main:login_view')

def login_view(request):
    if 'is_login' not in request.session:
        form = Login()
        if request.method == "POST":
            form = Login(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                password = form.cleaned_data['password']

                # check di tabel user
                with connection.cursor() as cursor:
                    cursor.execute(f"SELECT * FROM pengguna WHERE email='{email}' AND password='{password}'")
                    data = cursor.fetchone()

                    
                if data:
                    # cari tipe user
                    tipe = None
                    with connection.cursor() as cursor:
                        cursor.execute(f"SELECT * FROM pelanggan WHERE email='{email}'")
                        ispelanggan = cursor.fetchone()
                        if not ispelanggan:
                            cursor.execute(f"SELECT * FROM staf WHERE email='{email}'")
                            isStaf = cursor.fetchone()
                            if not isStaf:
                                tipe = 'Kurir'
                            else:
                                tipe = 'Staf'
                        else:
                            tipe = 'Pelanggan'
                    request.session['nama_lengkap'] = data[2]
                    request.session['user_type'] = tipe
                    request.session['email'] = tipe.upper() + data[0]
                    request.session['is_login'] = True

                    return redirect('hijau:read_laundry')
        return render(request, 'main/login.html', {'form':form})
    return redirect('hijau:read_laundry')

def logout_view(request):
    del request.session['nama_lengkap']
    del request.session['user_type']
    del request.session['is_login']
    return redirect('main:login_view')

def register_kurir(request):
    return render(request, 'register_kurir.html')

def register_pelanggan(request):
    return render(request, 'register_pelanggan.html')

def register_staf(request):
    return render(request, 'register_staf.html')

def register(request):
    return render(request, 'register.html')