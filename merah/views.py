from django.shortcuts import render
from django.shortcuts import redirect

# Create your views here.
def home(request):
    return render(request, 'fiturmerah.html')

def createtestimoni(request):
    return render(request, 'createtestimoni.html')

def ru_layanan(request):
    return render(request, 'ru_layanan.html')

def rud_testimoni(request):
    return render(request, 'rud_testimoni.html')

def updatelayanan(request):
    return render(request, 'updatelayanan.html')

def updatetestimoni(request):
    return render(request, 'updatetestimoni.html')