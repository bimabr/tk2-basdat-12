$(function() {
    // $('#id_tanggal_transaksi').addClass('form-control');
    const endpoint = '/hijau/laundry/create/';
    let time_delay = false;
    const csrf = JSON.parse(document.getElementById('csrf').textContent).hello;

    let ajax_call = function (endpoint, req_parameters) {
        $.ajax({
            method: "POST",
            url: endpoint,
            data: req_parameters
        })
        .done( response => {
            $('#id_tanggal_transaksi').replaceWith(response['template'])
        })
    }

    $('#id_email').on('keyup', function (event) {
        var keyword = $('#id_email').val();
        const req_parameters = {
            csrfmiddlewaretoken: csrf,
            email: keyword,
        }

        // Saat ada delay, proses pemanggilan yang menumpuk akan digagalkan
        // Untuk mencegah pencarian yang tidak sesuai karena adanya tumpukan
        // dari hasil pemanggilan beberapa keyword
        if (time_delay) {
            clearTimeout(time_delay);
        }

        // Set delay supaya loader bisa ditampilkan
        time_delay = setTimeout(ajax_call, 800, endpoint, req_parameters);
    });
});