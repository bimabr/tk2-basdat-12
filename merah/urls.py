from django.urls import path

from . import views

app_name = 'merah'

urlpatterns = [
    path('', views.home, name='home'),
    path('testimoni/rud', views.rud_testimoni, name='rud_testimoni'),
    path('layanan/ru', views.ru_layanan, name='ru_layanan'),
    path('testimoni/create', views.createtestimoni, name='createtestimoni'),
    path('testimoni/update', views.updatetestimoni, name='updatetestimoni'),
    path('layanan/update', views.updatelayanan, name='updatelayanan'),
]
