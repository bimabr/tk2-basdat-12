$(function() {
    // $('section').removeClass('hide');
    // $('#user-email').val(`{{ dum.email }}`);
});

function pop_up(el) {
    var s = el.getAttribute('data-id');
    $(`#update_${s}`).popup({
        pagecontainer: '#page',
        transition: 'all 300ms ease-in-out'
    });
}

function popup_delete(el) {
    var s = el.getAttribute('data-id');
    $(`#delete_${s}`).popup({
        pagecontainer: '#page',
        transition: 'all 300ms ease-in-out'
    });
}

function popup_button(el) {
    var s = el.getAttribute('data-button');

    $(`#update_${s}`).popup('hide');
    $(`#yakin_${s}`).popup('show');
    $.fn.popup.defaults.closeelement = `#yakin_${s}_close`;
}

function submit_form(el) {
    var s = el.getAttribute('data-id');
    $(`.formie_${s}`).submit();
}