from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.login_view, name='login_view'),
    path('logout/', views.logout_view, name='logout'),
    path('register/', views.register, name='register'),
    path('register/kurir', views.register_kurir, name='register_kurir'),
    path('register/staf', views.register_staf, name='register_staf'),
    path('register/pelanggan', views.register_pelanggan, name='register_pelanggan'),
]
