from django import forms
from django.db import connection
class CreateListLaundryForm(forms.Form):
    att = {'class':'form-control'}
    email = forms.EmailField(widget=forms.EmailInput(attrs=att))
    tanggal_transaksi = forms.DateTimeField(input_formats=["%Y/%m/%d %H:%M:%S"], widget=forms.Select(attrs=att, choices=[]))
    jumlah_item = forms.IntegerField(min_value=0, widget=forms.NumberInput(attrs=att))
    kode_item = forms.ChoiceField(choices=[], widget=forms.Select(attrs=att, choices=[]))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute('SELECT kode FROM item')
            lst = [ (i[0],i[0]) for i in cursor.fetchall() ]
            self.fields['kode_item'].choices = lst
            self.fields['kode_item'].widget.choices = lst

    