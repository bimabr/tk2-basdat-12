from django.urls import path

from . import views

app_name = 'biru'

urlpatterns = [
    path('', views.home, name='home'),
    path('qstaff', views.qstaff, name='qstaff'),
    path('qpelanggan', views.qpelanggan, name='qpelanggan'),
    path('qkurir', views.qkurir, name='qkurir'),
    path('staff', views.staff, name='staff'),
    path('update', views.update, name='update'),
    path('delete', views.delete, name='delete'),
    path('kurir', views.kurir, name='kurir'),
    path('create', views.create, name='create'),
    path('status', views.status, name='status'),
    path('pilih', views.pilihEmail, name='pilih'),
]
