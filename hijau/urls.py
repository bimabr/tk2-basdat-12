from django.urls import path

from . import views

app_name = 'hijau'

urlpatterns = [
    path('laundry/create/', views.home, name='home'),
    path('laundry/list/', views.read_daftar_laundry, name='read_laundry'),
    path('laundry/update/', views.update_laundry, name='update_laundry'),
    path('laundry/delete/', views.delete_laundry, name='delete_laundry'),
    path('item/create', views.create_item, name='create_item'),
    path('item/list', views.read_item, name='read_item')
]
