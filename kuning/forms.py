from django import forms

class TarifAntarJemputForm(forms.Form):

	kode = forms.CharField(label='Kode Tarif Antar Jemput', max_length=100)
	jarak_min = forms.IntegerField(label='Jarak Min (Km)')
	jarak_max = forms.IntegerField(label='Jarak Max (Km)')
	harga = forms.FloatField(label='Harga (Rupiah)')

	def __ini__(self, *args, **kwargs):
		super(TarifAntarJemputForm, self).__init__(*args, **kwargs)
		for field in self.fields:
			self.fields[field].widget.attrs['class'] = 'form-control'
		# self.fields['kode'].widget.attrs['class'] = 'form-control form-control-sm col-8'

class DummyRoleForm(forms.Form):
	pilihan = [('pelanggan', 'pelanggan'), ('staf', 'staf'), ('kurir', 'kurir')]
	dummy_role = forms.ChoiceField(label='Pilih role sebagai', choices=pilihan, 
			widget=forms.RadioSelect(attrs={'onchange': 'this.form.submit();'}))

	def __init__(self, *args, **kwargs):
	    super(DummyRoleForm, self).__init__(*args, **kwargs)
	    self.fields['dummy_role'].widget.attrs['class'] = 'form-check'
    

