from django.shortcuts import render, redirect
from django.contrib import messages
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core.paginator import Paginator, PageNotAnInteger
from django.db import connection
# from django.utils.timezone import localtime
# import datetime

from .forms import CreateListLaundryForm


import math;


# Create your views here.

def home(request):
    if 'is_login' in request.session:
        context = {
            'form' : CreateListLaundryForm(),
            'hasSubmitBefore' : False
        }
        if request.is_ajax():
            keyword = request.POST.get('email')
            context = {}
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT tanggal FROM transaksi_laundry WHERE email='{keyword}'")
                context['tgl'] = cursor.fetchall()
            html = render_to_string(
                template_name='daftar_laundry/list_tanggal.html',
                context = context
            )

            return JsonResponse(data={'template':html})
        if request.method == 'POST':
            form = CreateListLaundryForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                tgl = form.cleaned_data['tanggal_transaksi']
                tgl = tgl.replace(tzinfo=None)
                jumlah_item = form.cleaned_data['jumlah_item']
                kode_item = form.cleaned_data['kode_item']
                # hitung jumlah daftar laundry yg ada
                with connection.cursor() as cursor:
                    cursor.execute('SELECT COUNT(*) FROM daftar_laundry')
                    no_urut =  cursor.fetchone()[0] + 1
                query = f'INSERT INTO "daftar_laundry" VALUES  (\'{email}\', \'{tgl}\', {no_urut}, {jumlah_item}, \'{kode_item}\')'
                with connection.cursor() as cursor:
                    cursor.execute( query )
                context['hasSubmitBefore'] = True
                messages.success(request, 'Daftar laundry berhasil dibuat.')

        return render(request, 'daftar_laundry/create_list_laundry.html', context)
    return redirect('main:login_view')

def read_daftar_laundry(request):
    if 'is_login' in request.session :
        # jika bukan kurir maka dapat read daftar laundry
        if request.session['user_type'] != 'Kurir':
            with connection.cursor() as cursor:
                # ambil semua daftar laundry jika staf,
                # jika pelanggan, maka ambil berdasarkan email
                if request.session['user_type'] == 'Staf':
                    cursor.execute('SELECT * FROM daftar_laundry ORDER BY no_urut ASC')
                elif request.session['user_type'] == 'Pelanggan':
                    email = request.session['email'].replace('PELANGGAN', '')
                    cursor.execute(f'SELECT * FROM daftar_laundry WHERE email=\'{email}\' ORDER BY no_urut ASC')
                lists = cursor.fetchall()
                cursor.execute('SELECT kode FROM item')
                codes = cursor.fetchall()

            paginator = Paginator(lists, 5)
            page = request.GET.get('page')

            try:
                p = paginator.page(page)
            except PageNotAnInteger:
                p = paginator.page(1)
            email = request.session['email'].replace(request.session['user_type'].upper(), '')
            context = {
                'kode_list': [code[0] for code in codes],
                'isStaff' : request.session['user_type'] == 'Staf',
                'dummy' : p,
                'pelanggan_email' : email
            }
            if not context['isStaff']:
                c = lambda x : len([i for i in lists if i[0]==x])
                context['num_pesanan'] = c(context['pelanggan_email'])
                context['range'] = range(1,math.ceil(context['num_pesanan']/5+1 ))
            return render(request, 'daftar_laundry/read_list_laundry.html', context)
        return redirect('kuning:read_profil')
    return redirect('/login/')

def update_laundry(request):
    if request.method == 'POST':
        no_urut = request.POST['no_urut']
        j_item = request.POST['jumlah_item']
        k_item = request.POST['kode_item']
        # update daftar laundry
        with connection.cursor() as cursor:
            cursor.execute(f"UPDATE daftar_laundry SET jumlah_item={j_item}, kode_item='{k_item}' \
                            WHERE no_urut={no_urut}")
        return redirect('hijau:read_laundry')


def delete_laundry(request):
    if request.method == 'POST':
        no_urut = request.POST['no_urut']
        query = f'DELETE FROM daftar_laundry WHERE no_urut={no_urut}'
        with connection.cursor() as cursor:
            # dapatkan jumlah row di daftar_laundry
            cursor.execute('SELECT COUNT(*) FROM daftar_laundry')
            max = cursor.fetchone()[0]
            # jalankan query delete
            cursor.execute(query)

            # update no_urut dari data yg dihapus sampai data terakhir
            for i in range(no_urut, max):
                cursor.execute(f'UPDATE daftar_laundry SET no_urut=no_urut-1 WHERE no_urut={no_urut+1}')
        return redirect('hijau:read_laundry')

def read_item(request):
    if 'is_login' in request.session :
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM item')
            dummy = cursor.fetchall()
        context = {
            'dummy' : dummy
        }
        return render(request, 'item/read_item.html', context)
    return redirect('main:login_view')

def create_item(request):
    if 'is_login' in request.session:
        if request.session['user_type'] == 'Staf':
            if request.method == 'POST':
                query = f"INSERT INTO item VALUES ('{request.POST['kode_item']}','{request.POST['nama_item']}')"
                with connection.cursor() as cursor:
                    cursor.execute( query )
            return render(request, 'item/create_item.html')
        return redirect('kuning:read_profil')
    return redirect('/login/')