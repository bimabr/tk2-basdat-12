from django.urls import path

from . import views

app_name = 'kuning'

urlpatterns = [
    path('', views.home, name='home'),
    path('read_tarif', views.read_tarif, name='read_tarif'),
    path('create_tarif', views.create_tarif, name='create_tarif'),
    path('update_tarif/<slug:kode_update>', views.update_tarif, name='update_tarif'),
    path('delete_tarif/<slug:kode_delete>', views.delete_tarif, name='delete_tarif'),
    path('read_profil', views.read_profil, name='read_profil'),
]