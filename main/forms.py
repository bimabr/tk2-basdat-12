from django import forms
from django.db import connection

class Login(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(max_length = 50, widget=forms.PasswordInput())

class Register(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(max_length = 50, widget=forms.PasswordInput())

class CustomerRegister(forms.Form):
    email = forms.EmailField(max_length = 50)
    password = forms.CharField(max_length = 50, widget=forms.PasswordInput())
    namalengkap = forms.CharField(max_length = 50)
    notelepon = forms.CharField(max_length = 20)
    jeniskelamin = forms.CharField(max_length = 1, label = 'Type P for Perempuan, L for Laki-Laki')
    tanggallahir = forms.DateField()
    alamat = forms.CharField(max_length = 50)
    
class CourierRegister(forms.Form):
    email = forms.EmailField(max_length = 50)
    password = forms.CharField(max_length = 50, widget=forms.PasswordInput())
    namalengkap = forms.CharField(max_length = 50)
    notelepon = forms.CharField(max_length = 20)
    jeniskelamin = forms.CharField(max_length = 1, label = 'Type P for Perempuan, L for Laki-Laki')
    alamat = forms.CharField(max_length = 50, required = False)
    npwp = forms.CharField(max_length = 20)
    norekening = forms.CharField(max_length = 20)
    namabank = forms.CharField(max_length = 50)
    kantorcabang = forms.CharField(max_length = 50)
    nomorsim = forms.CharField(max_length = 20)
    nomorkendaraan = forms.CharField(max_length = 20)
    jeniskendaraan = forms.CharField(max_length = 20)

class StaffRegister(forms.Form):
    email = forms.EmailField(max_length = 50)
    password = forms.CharField(max_length = 50, widget=forms.PasswordInput())
    namalengkap = forms.CharField(max_length = 50)
    notelepon = forms.CharField(max_length = 20)
    jeniskelamin = forms.CharField(max_length = 1, label = 'Type P for Perempuan, L for Laki-Laki')
    alamat = forms.CharField(max_length = 50, required = False)
    npwp = forms.CharField(max_length = 20)
    norekening = forms.CharField(max_length = 20)
    namabank = forms.CharField(max_length = 50)
    kantorcabang = forms.CharField(max_length = 50)